import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-board",
  templateUrl: "./board.component.html",
  styleUrls: ["./board.component.css"]
})
export class BoardComponent implements OnInit {
  board: any[];
  currentPlayer: any;
  gameOver: boolean;
  gameText: string;
  player1 = { name: "Player 1", symbol: "o" };
  player2 = { name: "Player 2", symbol: "x" };

  constructor() {}

  ngOnInit() {
    this.newGame();
  }

  get winningCombinations(): any[] {
    return [
      [0, 1, 2], // top horizontal line
      [3, 4, 5], // middle horizontal line
      [6, 7, 8], // bottom horizontal line
      [0, 3, 6], // first vertical line
      [1, 4, 7], // second vertical line
      [2, 5, 8], // third vertical line
      [0, 4, 8], // back slash
      [2, 4, 6] // forward slash
    ];
  }

  newGame() {
    this.board = Array.from(Array(9), x => (x = { value: "", win: false }));
    this.gameOver = false;
    this.nextPlayer(this.player2);
  }

  onMove(square) {
    if (square.value === "" && !this.gameOver) {
      square.value = this.currentPlayer.symbol;
      this.completeMove(this.currentPlayer);
    }
  }

  completeMove(player) {
    if (this.isWinner(player.symbol)) {
      this.gameText = `${player.name} wins!`;
      this.gameOver = true;
    } else if (!this.canMove()) {
      this.gameText = "It's a draw!";
      this.gameOver = true;
    } else {
      this.nextPlayer(player);
    }
  }

  isWinner(symbol): boolean {
    return this.winningCombinations.some(combination => {
      const win = combination.every(
        square => this.board[square].value === symbol
      );
      if (win) {
        combination.forEach(square => {
          this.board[square].win = true;
        });
      }
      return win;
    });
  }

  canMove(): boolean {
    return this.board.filter(s => s.value === "").length > 0;
  }

  nextPlayer(player) {
    this.currentPlayer = player === this.player2 ? this.player1 : this.player2;
    this.gameText = `Ready ${this.currentPlayer.name}`;
  }
}
